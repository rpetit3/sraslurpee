import os
from bs4 import BeautifulSoup
from Bio import Entrez
import urllib2
Entrez.email = 'robert.petit@emory.edu'

class Query(object):

    def __init__(self, db='sra', retmax=100000):
        self.db = db
        self.retmax = retmax
        self.__eutils = 'http://www.ncbi.nlm.nih.gov/entrez/eutils/e'
        self.__ena = 'http://www.ebi.ac.uk/ena/data/view/reports/sra'
        self.runs = []
        self.__run_to_uid = {}
        self.__ena_info = []

    def search(self, term):
        ''' Query NCBI SRA using eSearch '''
        handle = Entrez.esearch(db=self.db, retmax=self.retmax, term=term, usehistory="y")
        record = Entrez.read( handle )
        self.uids = record['IdList']
        self.__webenv = record['WebEnv']
        self.__query_key = record['QueryKey'] 
        return record['Count']

    def get_run_acc(self, batch_size=5000):
        ''' Get the Run accessions for each UID '''
        count = len(self.uids)
        for start in xrange(0,count,batch_size):
            handle = Entrez.esummary(db=self.db, webenv=self.__webenv, 
                                     query_key=self.__query_key, 
                                     retstart=start, retmax=batch_size)
            records = Entrez.read(handle)
            for record in records:
                xml = BeautifulSoup(record['Runs'])
                tag = xml.run
                self.__run_to_uid[tag['acc']] = record['Id']
            handle.close()
        
    def get_ena_info(self):
        for run in self.__run_to_uid.keys():
            url = '{0}/fastq_files/{1}'.format(self.__ena, run)
            request = urllib2.Request(url)
            html = urllib2.urlopen(request).read()
            for line in html.split('\n'):
                '''0:Study               7:Instrument Model   14:Run Base Count 
                   1:Sample              8:Library Name       15:File Name
                   2:Experiment          9:Library Layout     16:File Size 
                   3:Run                 10:Library Strategy  17:md5
                   4:Analysis            11:Library Source    18:Ftp
                   5:Organism            12:Library Selection
                   6:Instrument Platform 13:Run Read Count                      '''
               
                if len(line) > 0:
                    if len(self.__ena_info) > 0 and line.startswith('Study'):
                        continue
                    else:
                        self.__ena_info.append(line)

    def write_ena_info(self):
        fh = open('ena_info.txt', 'w')
        fh.write('\n'.join(self.__ena_info))
        fh.close()
        
    def write_runs(self):
        fh = open('runs.txt', 'w')
        fh.write('UID\tRun_Acc\n')
        for k,v in self.__run_to_uid.items():
            fh.write('{0}\t{1}\n'.format(v,k))
        fh.close()          
    