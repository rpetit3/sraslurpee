#! /usr/bin/env python
import sys
from sraslurpee import query

if __name__ == '__main__':
    q = ['("Staphylococcus aureus"[Organism] NOT "Streptococcus pneumoniae"',
         '[orgn] NOT "unidentified"[orgn] NOT "Ostrya rehderiana"[orgn] NOT ',
         '"Staphylococcus warneri RN833"[orgn] NOT "Staphylococcus xylosus"',
         '[orgn]) NOT cluster_dbgap[PROP] AND cluster_public[PROP] AND ',
         '"sra_public"[Filter] AND "type_genome"[Filter] AND ',
         '"dna_data"[Filter]']
    query = query.Query(retmax=100000)
    query.search(''.join(q))
    print 'Total hits: {0}'.format(len(query.uids))
    
    # UID --> Run
    query.get_run_acc(10)
    # Run --> ENA
    query.get_ena_info()
    
    #Write outputs
    query.write_runs()
    query.write_ena_info()