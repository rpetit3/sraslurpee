#! /usr/bin/env python
"""
    Robert A. Petit III
    7/2/2013
    
    Python Libraries: 
        - subprocess
        
    Program Requirements:
        - Sun Grid Engine
        - SRA Tool Kit (fastq-dump)

"""
def read_locations(locations):
    locs = {}
    f = open(locations, 'r')
    while (1):
        line = f.readline().rstrip()
        if not line:
            break
        
        # 0=SRR, 1=Location
        cols = line.split('\t')
        locs[ cols[0] ] = cols[1]
            
    f.close()
    
    return locs

def read_accessions(accessions):
    acc = {}
    f = open(accessions, 'r')
    while (1):
        line = f.readline().rstrip()
        if not line:
            break
        # 0=ID, 1=SRR, 2=SRX, 3=SRS
        cols = line.split('\t')
        
        acc[ cols[1] ] = [ cols[2], cols[3] ]
            
    f.close()
    
    return acc

def fastq_dump(sra, out, run):
    """ Convert the sra formated file into fastq. """
    import subprocess
    
    dumpFH = open('/home/rpetit/rpdata/sra_staph/tmp/fastq-dump.sh', 'w')
    dumpFH.write('#!/bin/bash\n');
    dumpFH.write('### Change to the current working directory:\n')
    dumpFH.write('#$ -wd /home/rpetit/rpdata/sra_staph/tmp/\n')
    dumpFH.write('### Job name:\n')
    dumpFH.write('#$ -o /home/rpetit/rpdata/sra_staph/logs/'+ run +'.out\n')
    dumpFH.write('#$ -e /home/rpetit/rpdata/sra_staph/logs/'+ run +'.err\n')
    dumpFH.write('#$ -N '+ run +'\n')
    dumpFH.write('#$ -S /bin/bash\n')
    dumpFH.write('mkdir -p '+ out +'\n')
    dumpFH.write('perl /usr/local/bin/configuration-assistant.perl ' + sra + '\n')
    dumpFH.write('/usr/local/bin/fastq-dump '+ sra +' -O '+ out +' --accession '+ run +' --minReadLen 20 --split-spot --skip-technical --helicos --dumpbase --bzip2\n')
    dumpFH.write('rm -r '+ sra +'\n')
    dumpFH.close()
    
    p = subprocess.Popen(['qsub', '/home/rpetit/rpdata/sra_staph/tmp/fastq-dump.sh'], 
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out,err = p.communicate()
    print out

if __name__ == '__main__':
    import os
    import sys
    import argparse as ap
    
    parser = ap.ArgumentParser(prog='SRAslurpee', conflict_handler='resolve', 
                               description="SRAslurpee - Given a SRA query, slurp all associated .(c)sra files.")
    group1 = parser.add_argument_group('Options', '')
    group1.add_argument('-l', '--locations', required=True, metavar="STRING",
                                             help='A text file containing the location of each .sra file.')
    group1.add_argument('-a', '--accessions', required=True, metavar="STRING",
                                        help='A text file containing run, experiment, and sample accessions.')
    group1.add_argument('-o', '--outdir', help='Output directory. (Default: ./fastq_dump)', metavar="STRING")
    group1.add_argument('-v', '--verbose', action='store_true', help='Produce status updates of the run.') 
    group1.add_argument('-h', '--help', action='help', help='Show this help message and exit')
    group1.add_argument('--version', action='version', version='%(prog)s v0.1', 
                        help='Show program\'s version number and exit')

    if len(sys.argv) == 1:
        parser.print_usage()
        sys.exit(1)

    args = parser.parse_args()
    
    # If no outdir, use the given name as the outdir
    if not args.outdir:
        args.outdir = './fastq_dump'
    
    if not os.path.exists(args.locations):
        print "Unable to use "+ args.locations +", please verify the file exists."
        sys.exit(1);
    
    if not os.path.exists(args.accessions):
        print "Unable to use "+ args.accessions +", please verify the file exists."
        sys.exit(2);
    
    locations = read_locations(args.locations)
    accessions = read_accessions(args.accessions)
    
    for run in accessions:
        print 'Submitting ' + run
        sra = locations[run]
        exp = accessions[run][0]
        sample = accessions[run][1]
        out = '/home/rpetit/rpdata/sra_staph/Samples/'+ sample +'/'+ exp  +'/'
        
        fastq_dump(sra, out, run)  
    