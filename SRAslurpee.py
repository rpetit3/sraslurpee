#! /usr/bin/env python
"""
    Robert A. Petit III
    6/21/2013
    
    Python Libraries: 
        - re, subprocess, & urllib
        
    Program Requirements:
        - Aspera Connect Client
            - http://downloads.asperasoft.com/download_connect/
        - Add NCBI to trusted resources list:
            - Example: 
                ascp -i asperaweb_id_dsa.putty -Q  -l400m anonftp@ftp-private.ncbi.nlm.nih.gov:/1GB /dev/null

"""
import urllib
import re
import subprocess
import os

class REMatcher(object):
    def __init__(self, matchstring):
        self.matchstring = matchstring

    def match(self,regexp):
        self.rematch = re.match(regexp, self.matchstring)
        return bool(self.rematch)

    def group(self,i):
        return self.rematch.group(i)

class slurpSRA():
  
  def __init__(self, outdir, query, verbose, ascp, identity):
    self.__outdir     = outdir
    self.__query      = query
    self.__ascp       = ascp
    self.__identity   = identity
    self.__ids        = []
    self.__publicRuns = []
    self.__UTILS      = 'http://www.ncbi.nlm.nih.gov/entrez/eutils'
    self.__retmax     = 200000
    self.__logged     = True
    self.CallCommand('mkdirFirst',['mkdir', '-p', self.__outdir]);
    self.__logFH      = open(self.__outdir + "/SRAslurpee.log", 'a+')
    self.CallCommand('mkdir',['mkdir', '-p', self.__outdir + '/logs']);
    self.CallCommand('mkdir',['mkdir', '-p', self.__outdir + '/tmp']);
    self.CallCommand('mkdir',['mkdir', '-p', self.__outdir + '/Samples']);
    self.__allFH      = open(self.__outdir + '/Summary.txt', 'w')
    self.__runFH      = open(self.__outdir + '/Runs.txt', 'w')
    self.__runToExp   = open(self.__outdir + '/Run2Exp.txt', 'w')
    self.__wgetFH     = open(self.__outdir + '/Downloads.txt', 'w')
        
    self.eSearch()
    for i in range(len(self.__ids)):
      print 'Working on '+ str(i+1) +' of '+ str(len(self.__ids))
      self.__reInit(self.__ids[i])
      self.eLink(self.__ids[i])
      self.eSummary(self.__ids[i])
      self.printAll()
      self.printRunToExp()
      self.downloadRuns()
      
  def __reInit(self, id):
    """ Reset the experiment values to empty strings. """
    self.__uid          = id
    self.__title        = ''
    self.__instrument   = ''
    self.__platform     = ''
    self.__publicRuns   = []
    self.__totalRuns    = ''
    self.__totalSpots   = ''
    self.__totalBases   = ''
    self.__totalSize    = ''
    self.__loadDone     = ''
    self.__clusterName  = ''
    self.__submitterAcc = ''
    self.__centerName   = ''
    self.__contactName  = ''
    self.__labName      = ''
    self.__expAcc       = ''
    self.__expVersion   = ''
    self.__expStatus    = ''
    self.__expName      = ''
    self.__studyAcc     = ''
    self.__studyName    = ''
    self.__taxID        = ''
    self.__sciName      = ''
    self.__cmnName      = ''
    self.__sampleAcc    = ''
    self.__sampleName   = ''
    self.__libName      = ''
    self.__libStrategy  = ''
    self.__libSource    = ''
    self.__libSelection = ''
    self.__libConstruct = ''
    self.__createDate   = ''
    self.__updateDate   = ''
    self.__output       = ''
    self.__pmids        = []
    self.__isPublished  = False
    self.__pubMed       = ''
    
  def eSearch(self):
    """ Query NCBI using eSearch """
    q = self.__UTILS + '/esearch.fcgi?db=sra&retmax=' + str(self.__retmax) + '&term=' + self.__query
    f = urllib.urlopen(q)
    l = f.read().split('\n')
    f.close()
    for i in range(len(l)):
      m = REMatcher(l[i])
      if (m.match(r'^\s+<Id>(\d+)</Id>')):
        self.__ids.append(m.group(1))
    
  def eSummary(self, id):
    """ Get summary information for a given id using eSummary """
    q = self.__UTILS + '/esummary.fcgi?db=sra&id=' + id
    f = urllib.urlopen(q)
    pat = re.compile(r'\t+')
    
    # Remove whitespace, then substitute >/<
    s = pat.sub('', f.read())
    f.close()
    r = s.replace('&lt;', '<')
    r = r.replace('&gt;', '>')
    r = r.replace('><', '>><<').split('><')
    
    # Parse the summary
    for i in range(len(r)):
      #print r[i]
      self.parseSummary(r[i])
      
  def eLink(self, id):
    """ Determine if the given id has a link to PubMed. """
    q = self.__UTILS + '/elink.fcgi?dbfrom=sra&db=pubmed&id=' + id
    f = urllib.urlopen(q)
    l = f.read().split('\n')
    f.close()
    for i in range(len(l)):
      m = REMatcher(l[i])
      if (m.match(r'^\s+<Id>(\d+)</Id>')):
        if (m.group(1) != id):
          self.__pmids.append(m.group(1))
          self.__isPublished = True
          
    if (self.__isPublished):
      self.__pubMed = 'Pubmed=' + ':'.join(self.__pmids)
    else:
      self.__pubMed = 'Unpublished'
    
  def parseSummary(self, line):
    """ Parse the input line and set the values """
    m = REMatcher(line)
    if (m.match(r'<Id>(.*)</Id>')):
      self.__uid     = m.group(1)
    elif (m.match(r'<Title>(.*)</Title>')):
      self.__title   = m.group(1)
    elif (m.match(r'<Platform instrument_model="(.*)">(.*)</Platform>')):
      self.__instrument = m.group(1)
      self.__platform   = m.group(2)
      if not os.path.exists(self.__outdir + '/tmp/'+ self.__platform + '/'):
        self.CallCommand('mkdir', ['mkdir', '-p', self.__outdir + '/tmp/'+ self.__platform + '/'])
    elif (m.match(r'<Statistics total_runs="(.*)" total_spots="(.*)" total_bases="(.*)" total_size="(.*)" load_done="(.*)" cluster_name="(.*)"/>')):
      self.__totalRuns   = m.group(1)
      self.__totalSpots  = m.group(2)
      self.__totalBases  = m.group(3)
      self.__totalSize   = m.group(4)
      self.__loadDone    = m.group(5)
      self.__clusterName = m.group(6)
    elif (m.match(r'<Submitter acc="(.*)" center_name="(.*)" contact_name="(.*)" lab_name="(.*)"/>')):
      self.__submitterAcc = m.group(1)
      self.__centerName   = m.group(2)
      self.__contactName  = m.group(3)
      self.__labName      = m.group(4)
    elif (m.match(r'<Experiment acc="(.*)" ver="(.*)" status="(.*)" name="(.*)"/>')):
      self.__expAcc     = m.group(1)
      self.__expVersion = m.group(2)
      self.__expStatus  = m.group(3)
      self.__expName    = m.group(4)
    elif (m.match(r'<Study acc="(.*)" name="(.*)"/>')):
      self.__studyAcc  = m.group(1)
      self.__studyName = m.group(2)
    elif (m.match(r'<Organism taxid="(.*)" ScientificName="(.*)" CommonName="(.*)"/>')):
      if (m.group(1) != '32644'):
        self.__taxID   = m.group(1)
        self.__sciName = m.group(2)
        self.__cmnName = m.group(3)
    elif (m.match(r'<Organism taxid="(.*)" ScientificName="(.*)"/>')):
      if (m.group(1) != '32644'):
        self.__taxID   = m.group(1)
        self.__sciName = m.group(2)
    elif (m.match(r'<Organism taxid="(.*)" CommonName="(.*)"/>')):
      if (m.group(1) != '32644'):
        self.__taxID   = m.group(1)
        self.__cmnName = m.group(2)
    elif (m.match(r'<Sample acc="(.*)" name="(.*)"/>')):
      self.__sampleAcc  = m.group(1)
      self.__sampleName = m.group(2)
    elif (m.match(r'<LIBRARY_NAME>(.*)</LIBRARY_NAME>')):
      self.__libName = m.group(1)
    elif (m.match(r'<LIBRARY_STRATEGY>(.*)</LIBRARY_STRATEGY>')):
      self.__libStrategy = m.group(1)
    elif (m.match(r'<LIBRARY_SOURCE>(.*)</LIBRARY_SOURCE>')):
      self.__libSource = m.group(1)
    elif (m.match(r'<LIBRARY_SELECTION>(.*)</LIBRARY_SELECTION>')):
      self.__libSelection = m.group(1)
    elif (m.match(r'<LIBRARY_CONSTRUCTION_PROTOCOL>(.*)</LIBRARY_CONSTRUCTION_PROTOCOL>')):
      self.__libConstruct  = m.group(1)
    elif (m.match(r'<Run acc="(.*)" total_spots="(.*)" total_bases="(.*)" load_done="(.*)" is_public="(.*)" cluster_name="(.*)" static_data_available="(.*)"/>')):
      self.printRun(self.__uid, m.group(1), m.group(2), m.group(3), m.group(4), m.group(5), m.group(6), m.group(7))
      if (m.group(6) == "public"):
        self.__publicRuns.append(m.group(1))
    elif (m.match(r'<Item Name="CreateDate" Type="String">(.*)</Item>')):
      self.__createDate = m.group(1)
    elif (m.match(r'<Item Name="UpdateDate" Type="String">(.*)</Item>')):
      self.__updateDate = m.group(1)
      
  def downloadRuns(self):
    """ Go to SRA and retrieve the run. """
    for i in range(len(self.__publicRuns)):
      run = self.__publicRuns[i]
      print '   Working on '+ run +' (' + str(i+1) +' of '+ str(len(self.__publicRuns))+ ')'
      url  = 'anonftp@ftp-private.ncbi.nlm.nih.gov:/sra/sra-instant/reads/ByRun/sra/'
      url += run[0:3] +'/'+ run[0:6] +'/'+ run +'/'+ run + '.sra'
      sra = self.__outdir + '/tmp/'+ self.__platform + '/' + run +'.sra'
      exists = ''
      
      if not os.path.exists(sra):
        self.CallCommand('ascp', [self.__ascp, '-i', self.__identity, '-Q', '-l450m', url, 
                                  self.__outdir + '/tmp/' + self.__platform + '/'])
        exists = run +'.sra Downloaded'
      else:
        exists = run +'.sra Already existed'
      
      if not os.path.exists(sra):
        url  = 'anonftp@ftp-private.ncbi.nlm.nih.gov:/sra/sra-instant/reads/ByRun/sra/'
        url += run[0:3] +'/'+ run[0:6] +'/'+ run +'/'+ run + '.csra'
        sra = self.__outdir + '/tmp/'+ self.__platform + '/' + run +'.csra'
        
        if not os.path.exists(sra):
          sra = self.__outdir + '/tmp/'+ self.__platform + '/' + run +'.csra'
          self.CallCommand('ascp', [self.__ascp, '-i', self.__identity, '-Q', '-l450m', url, 
                                    self.__outdir + '/tmp/' + self.__platform + '/'])
          exists = run +'.csra Downloaded'
        else:
          exists = run +'.csra Already existed'
      
      print '      - File ' + exists
        
      if not os.path.exists(sra):
        self.__wgetFH.write(self.__uid +'\tError Please Check\t'+ sra +'\t'+ url +'\n')
  
  def printAll(self):
    """ Print all of the summary information. """
    #  0) Experiment Accession    10) Experiment Name         20) Submitting Center Contact     30) Cluster Name
    #  1) SRA UID                 11) Library Name            21) Submitting Lab                31) Create Date
    #  2) Title                   12) Library Strategy        22) Experiment Version            32) Update Date
    #  3) Sample Accession        13) Library Source          23) Experiment Status             33) Publication
    #  4) Taxon ID                14) Library Selection       24) Total Runs
    #  5) Scientific Name         15) Library Contruct        25) Total Public Runs
    #  6) Common Name             16) Instrument              26) Total Spots
    #  7) Sample Name             17) Platform                27) Total Bases   
    #  8) Study Accession         18) Submitter Accession     28) Total Size 
    #  9) Study Name              19) Submitting Center       29) Load Done
    
    output = [ self.__expAcc, self.__uid, self.__title, self.__sampleAcc, self.__taxID, self.__sciName, self.__cmnName, 
               self.__sampleName, self.__studyAcc, self.__studyName, self.__expName, self.__libName, self.__libStrategy,
               self.__libSource, self.__libSelection, self.__libConstruct, self.__instrument, self.__platform,  
               self.__submitterAcc, self.__centerName, self.__contactName, self.__labName, self.__expVersion, 
               self.__expStatus, self.__totalRuns, str(len(self.__publicRuns)), self.__totalSpots, self.__totalBases, 
               self.__totalSize, self.__loadDone, self.__clusterName, self.__createDate, self.__updateDate, self.__pubMed ]
    o = '\t'.join(output)
    self.__allFH.write(o + '\n')
    
  def CallCommand(self, program, command):
    """ Allows execution of a simple command. """
    r   = 1
    out = ""
    err = ""
    disregard = ['wget', 'qsub', 'mkdirFirst']
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out,err = p.communicate()
    
    if (type(program) is list):
      if len(program) == 3:
        r = out
      else:
        o = open(program[1], 'w')
        o.write(out)
        o.close()
      program = program[0]
      out = ""
      
    if (self.__logged and program not in disregard):
      self.__logFH.write('---[ '+ program +' ]---\n')
      self.__logFH.write('Command: \n' + ' '.join(command) + '\n\n')
      if (len(out) > 0):
        self.__logFH.write('Standard Output: \n' + out + '\n\n')
      if (len(err) > 0):
        self.__logFH.write('Standard Error: \n' + err + '\n\n')
    elif (program == 'mkdirFirst'):
      p = subprocess.Popen(['readlink', '-f', self.__outdir], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      out,err = p.communicate()
      self.__outdir = out.rstrip()
      
    return r
    
  def printRun(self, id, acc, spots, bases, load, public, cluster, static):
    """ Print run information. """
    self.__runFH.write('ID=' + id + '\tRunACC=' + acc + '\tRunTotalSpots=' + spots + '\tRunTotalBases=' + bases + '\tRunLoadDone=' + load)
    self.__runFH.write('\tRunIsPublic=' + public + '\tRunClusterName=' + cluster + '\tRunAvailable=' + static + '\n')
    
  def printRunToExp(self):
    """ Print Run to Experiment information. """
    for i in range(len(self.__publicRuns)):
      self.__runToExp.write(self.__uid + '\t' + self.__publicRuns[i] + '\t' + self.__expAcc + '\t' + self.__sampleAcc + '\n')


if __name__ == '__main__':
    import sys
    import argparse as ap
    
    parser = ap.ArgumentParser(prog='SRAslurpee', conflict_handler='resolve', 
                               description="SRAslurpee - Given a SRA query, slurp all associated .(c)sra files.")
    group1 = parser.add_argument_group('Options', '')
    group1.add_argument('-q', '--query', required=True, help='A query against the SRA database.', metavar="STRING")
    group1.add_argument('-a', '--ascp', required=True, metavar="STRING",
                                        help='Location of ascp. (Example: /home/rpetit/.aspera/connect/bin/ascp )')
    group1.add_argument('-i', '--identity', required=True, metavar="STRING",
                                         help='''Location of the identity file used by ascp for NCBI connections. 
                                                 (Example: /home/rpetit/.aspera/connect/etc/asperaweb_id_dsa.putty )''')
    group1.add_argument('-o', '--outdir', help='Output directory. (Default: ./SRAslurpee)', metavar="STRING")
    group1.add_argument('-v', '--verbose', action='store_true', help='Produce status updates of the run.') 
    group1.add_argument('-h', '--help', action='help', help='Show this help message and exit')
    group1.add_argument('--version', action='version', version='%(prog)s v0.1', 
                        help='Show program\'s version number and exit')

    if len(sys.argv) == 1:
        parser.print_usage()
        sys.exit(1)

    args = parser.parse_args()
    
    # If no outdir, use the given name as the outdir
    if not args.outdir:
        args.outdir = './SRAslurpee'
    
    if not os.path.exists(args.ascp):
        print "Verify the full path for 'ascp' exists and has proper permisssions set."
        sys.exit(1);
    
    if not os.path.exists(args.identity):
        print "Verify the full path for the identity file exists."
        sys.exit(2);
    
    SRA = slurpSRA(args.outdir, args.query, args.verbose, args.ascp, args.identity)
